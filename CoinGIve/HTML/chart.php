<?php
 $dbhandle = new mysqli('localhost','root','','<DATABASENAME>' );
 echo $dbhandle->connect_error;
 
 $query = "select * from <TABLENAME>";
 $res=dbhandle->query($query);
 ?>
<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Charity', 'Donation Recieved'],
          <?php
		  
		  while($row-$res->fetch_assoc())
		  {
			  echo "['".$row['<ROWNAME>']."',".$row['<ROWNAME>']."],";
		  }
		  
		  ?>
        ]);

        var options = {
          title: 'Donation Reports',
          is3D: true,S
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="piechart_3d" style="width: 900px; height: 500px;"></div>
  </body>
</html>